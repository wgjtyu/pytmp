# -*- coding: utf-8 -*-
import os
from jinja2 import Environment, PackageLoader

env = Environment(loader=PackageLoader('main', 'templates'))
# env.trim_blocks = True
env.filters['removePrimaryKey'] = lambda rows: [x for x in rows if x.primaryKey != True]

# 用于生成JSP页面表单
env.filters['getNormalRow'] = lambda rows: [r for r in rows if r.primaryKey != True and \
                                            r.javaName not in ('editorId', 'editorName', 'createTime')]
env.filters['getSearch'] = lambda rows: [x for x in rows if x.search == True]
getVarName = lambda w: w[0].lower() + w[1:]
env.filters['getVarName'] = getVarName # 将每个单词首字母大写的字符串中，第一个字母转为小写
env.filters['isTwo'] = lambda n: n % 2 == 0

def checkDir():
    if not os.path.exists('result/entity/mapper'):
        os.makedirs('result/entity/mapper')
    if not os.path.exists('result/vo'):
        os.makedirs('result/vo')
    if not os.path.exists('result/service/impl'):
        os.makedirs('result/service/impl')
    if not os.path.exists('result/jsp'):
        os.makedirs('result/jsp')

def generateFile(table):
    checkDir()
    def parse(templatePath, savePath):
        template = env.get_template(templatePath)
        result = template.render(config = table, rows = table.rows)
        f = open('result/' + savePath, 'w')
        f.write(result)
        f.close()

    parse('entity/Entity.java', 'entity/{0}.java'.format(table.beanName))
    parse('vo/VO.java', 'vo/{0}.java'.format(table.voName))
    parse('entity/mapper/EntityMapper.xml', 'entity/mapper/{0}Mapper.xml'.format(table.beanName))
    parse('entity/mapper/EntityMapper.java', 'entity/mapper/{0}Mapper.java'.format(table.beanName))
    parse('service/Service.java', 'service/{0}Service.java'.format(table.voName))
    parse('service/impl/ServiceImpl.java', 'service/impl/{0}ServiceImpl.java'.format(table.voName))
    parse('Controller.java', '{0}Controller.java'.format(table.voName))
    parse('jsp/add.jsp' , 'jsp/{0}-add.jsp'.format(getVarName(table.voName)))
    parse('jsp/edit.jsp', 'jsp/{0}-edit.jsp'.format(getVarName(table.voName)))
    parse('jsp/list.jsp', 'jsp/{0}-list.jsp'.format(getVarName(table.voName)))
    parse('jsp/view.jsp', 'jsp/{0}-view.jsp'.format(getVarName(table.voName)))
