table = {}

table.RowModel = (data) ->
    this.dbName      = data.dbName
    this.javaName    = m.prop data.javaName
    this.dbType      = data.dbType
    this.javaType    = m.prop data.javaType
    this.description = m.prop(data.description)
    this.allowEmpty  = m.prop data.allowEmpty
    this.length      = data.length
    null # 防止coffee编译后破坏构造函数

table.TableModel = (data) ->
    this.dbName = data.name
    this.beanName = m.prop data.beanName
    this.type = m.prop data.type
    this.voName = m.prop data.voName
    this.description = m.prop data.description
    null

table.controller = ->
    this.list = new Array()
    m.request({method: "GET", url: "/db/table/#{m.route.param('tableId')}"}).then (response) =>
        this.tableInfo = new table.TableModel(response.table)
        response.rows.map (i) =>
            this.list.push new table.RowModel(i)

    this.save = (i) =>
        =>
            console.log 'save', i
            console.log this.list[i]
            m.request method: "POST", url: "/db/table/#{m.route.param('tableId')}/save", data: this.list[i]

    this.gen = ->
        console.log 'gen'
        m.request({method: "GET", url: "/generate/#{m.route.param('tableId')}"}).then (resp) =>
            console.log resp.voName
            console.log resp.beanName
            m.route "/download/#{resp.voName}/#{resp.beanName}"

    this.reset = ->
        console.log 'reset', m.route.param('tableId')

    this.updateTable = (fieldName) =>
        (event) =>
            value = event.target.value.trim()
            if value == this.tableInfo[fieldName]() then return
            this.saving true
            postData = {field: fieldName, val: value}
            this.tableInfo[fieldName](value)
            m.request({method: "POST", url: "/update/table/#{this.tableInfo.dbName}", data: postData}).then (resp) =>
                this.saving false

    this.saving = m.prop false

    this

table.view = (ctrl) ->
    m "html", [
        m "body", [
            m "div#button-group", [
                m "button.btn.btn-success", {onclick: ctrl.gen, disabled: ctrl.saving()}, "生成"
                m "button.btn.btn-danger", {onclick: ctrl.reset}, "重置"
            ]
            m "div#edit-table-info.form-horizontal", [
                m "label.control-label", ["Entity名",
                    m "input", {
                        type: 'text'
                        class: 'form-control'
                        onchange: ctrl.updateTable('beanName')
                        value: ctrl.tableInfo.beanName()
                    }
                ]
                m "label.control-label", ["VO名",
                    m "input", {
                        type: 'text'
                        class: 'form-control'
                        onchange: ctrl.updateTable('voName')
                        value: ctrl.tableInfo.voName()
                    }
                ]
                m "label.control-label", ["分类编码",
                    m "input", {
                        type: 'text'
                        class: 'form-control'
                        onchange: ctrl.updateTable('type')
                        value: ctrl.tableInfo.type()
                    }
                ]
                m "label.control-label", ["描述",
                    m "input", {
                        type: 'text'
                        class: 'form-control'
                        onchange: ctrl.updateTable('description')
                        value: ctrl.tableInfo.description()
                    }
                ]
            ]
            m "table.center-container.table.table-striped", [
                m "thead", [m "tr", [
                        m "td", "序号"
                        m "td", "列名(db)"
                        m "td", "列名(java)"
                        m "td", "列类型(db)"
                        m "td", "列类型(java)"
                        m "td", "描述"
                        m "td", "长度"
                        m "td", "保存"
                ]]
                m "tbody", [
                    ctrl.list.map (i, index)->
                        m "tr", [
                            m "td", index
                            m "td", i.dbName
                            m "td", [
                                m "input", {
                                    class: 'form-control'
                                    onkeyup: m.withAttr("value", i.javaName)
                                    value: i.javaName()
                                }
                            ]
                            m "td", i.dbType
                            m "td", [
                                m "input", {
                                    class: 'form-control'
                                    onkeyup: m.withAttr("value", i.javaType)
                                    value: i.javaType()
                                }
                            ]
                            m "td", [
                                m "input", {
                                    class: 'form-control'
                                    onkeyup: m.withAttr("value", i.description)
                                    value: i.description()
                                }
                            ]
                            m "td", i.length
                            m "td", [
                                m "button.btn.btn-success", {onclick: ctrl.save(index)}, "保存"
                            ]
                        ]
                ]
            ]
        ]
    ]
