setTimeout ->
    m.route.mode = 'pathname'
    m.route document.body, "/", {
        "/": db
        "/table/:tableId": table
        "/download/:voName/:beanName": download
    }
