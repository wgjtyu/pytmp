package cn.com.harme.service;

import cn.com.harme.framework.exception.ServiceException;
import cn.com.harme.vo.User;
import cn.com.harme.vo.search.BaseConditionVO;
import cn.com.harme.vo.{{config.voName}};

import java.util.List;

public interface {{config.voName}}Service extends AbstractBusinessObjectService {

    public String SERVICE_NAME = "{{config.voName|getVarName}}Service";

    public void add({{config.voName}} {{config.voName|getVarName}}, User loginUser) throws ServiceException;

    public void update({{config.voName}} {{config.voName|getVarName}}, User logineUser) throws ServiceException;

    public {{config.voName}} get({{config.primaryRow.javaType}} {{config.primaryRow.javaName}}) throws ServiceException;

    public void delete({{config.primaryRow.javaType}} {{config.primaryRow.javaName}}, User loginUser) throws ServiceException;

    public List<{{config.voName}}> search(BaseConditionVO vo);

    public List<{{config.voName}}> findAlls();

}
