package cn.com.harme.web.{{config.type}};

import cn.com.harme.common.util.ObjectUtils;
import cn.com.harme.framework.exception.ServiceException;
import cn.com.harme.service.{{config.voName}}Service;
import cn.com.harme.vo.search.ConditionVO;
import cn.com.harme.vo.{{config.voName}};
import cn.com.harme.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * {{config.description}}
 */
@Controller("{{config.type}}.{{config.voName|getVarName}}Controller")
@RequestMapping(value = "/{{config.type}}/{{config.voName|getVarName}}")
public class {{config.voName}}Controller extends BaseController {
    @Autowired
    private {{config.voName}}Service {{config.voName|getVarName}}Service;

    @RequestMapping("")
    public ModelAndView list(ConditionVO vo) {
        logger.debug("list param[vo=" + vo + "]");

        List<{{config.voName}}> {{config.voName|getVarName}}List = {{config.voName|getVarName}}Service.search(vo);
        if (ObjectUtils.isEmpty({{config.voName|getVarName}}List) && vo.getTotalCount() > 0) {
            vo.setPageNum(vo.getMaxPageNum());
            {{config.voName|getVarName}}List = {{config.voName|getVarName}}Service.search(vo);
        }

        ModelAndView model = new ModelAndView("/{{config.type}}/{{config.voName|getVarName}}-list");

        model.addObject("{{config.voName|getVarName}}List", {{config.voName|getVarName}}List);
        model.addObject("vo", vo);

        return model;
    }

    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView add() {
        logger.debug("add param[]");
        ModelAndView model = new ModelAndView("/{{config.type}}/{{config.voName|getVarName}}-add");
        return model;
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ModelAndView insert({{config.voName}} {{config.voName|getVarName}}) {
        logger.debug("insert param[{{config.voName|getVarName}}=" + {{config.voName|getVarName}} + "]");

        try {
            {{config.voName|getVarName}}Service.add{{config.voName}}({{config.voName|getVarName}}, getContextUser());
        } catch (ServiceException e) {
            return ajaxDoneError(e.getMessage());
        }

        return ajaxDoneSuccess(getMessage("msg.operation.success"));
    }

    @RequestMapping(value = "/edit/{{'{' + config.primaryRow.javaName + '}'}}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable("{{config.primaryRow.javaName}}") {{config.primaryRow.javaType}} {{config.primaryRow.javaName}}) {
        logger.debug("edit param[{{config.primaryRow.javaName}}=" + {{config.primaryRow.javaName}}+ "]");

        {{config.voName}} {{config.voName|getVarName}} = null;
        try {
            {{config.voName|getVarName}} = {{config.voName|getVarName}}Service.get{{config.voName}}({{config.primaryRow.javaName}});
        } catch (ServiceException e) {
            return ajaxDoneError(e.getMessage());
        }

        ModelAndView model = new ModelAndView("/{{config.type}}/{{config.voName|getVarName}}-edit");
        model.addObject("{{config.voName|getVarName}}", {{config.voName|getVarName}});

        return model;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView update({{config.voName}} {{config.voName|getVarName}}) {
        logger.debug("update param[{{config.voName|getVarName}}=" + {{config.voName|getVarName}} + "]");

        try {
            {{config.voName|getVarName}}Service.update{{config.voName}}({{config.voName|getVarName}}, getContextUser());
        } catch (ServiceException e) {
            return ajaxDoneError(e.getMessage());
        }

        return ajaxDoneSuccess(getMessage("msg.operation.success"));
    }

    @RequestMapping("/delete/{{'{' + config.primaryRow.javaName + '}'}}")
    public ModelAndView delete(@PathVariable("{{config.primaryRow.javaName}}") {{config.primaryRow.javaType}} {{config.primaryRow.javaName}}) {
        logger.debug("delete param[{{config.primaryRow.javaName}}=" + {{config.primaryRow.javaName}}+ "]");

        try {
            {{config.voName|getVarName}}Service.delete{{config.voName}}({{config.primaryRow.javaName}}, getContextUser());
        } catch (ServiceException e) {
            return ajaxDoneError(e.getMessage());
        }

        return ajaxDoneSuccess(getMessage("msg.operation.success"));
    }

    @RequestMapping(value = "/view/{{'{' + config.primaryRow.javaName + '}'}}", method = RequestMethod.GET)
    public ModelAndView view(@PathVariable("{{config.primaryRow.javaName}}") {{config.primaryRow.javaType}} {{config.primaryRow.javaName}}) {
        logger.debug("view param[{{config.primaryRow.javaName}}=" + {{config.primaryRow.javaName}}+ "]");

        ModelAndView model = new ModelAndView("/{{config.type}}/{{config.voName|getVarName}}-view");
        try {
            {{config.voName}} {{config.voName|getVarName}} = {{config.voName|getVarName}}Service.get{{config.voName}}({{config.primaryRow.javaName}});
            model.addObject("{{config.voName|getVarName}}", {{config.voName|getVarName}});
        } catch (ServiceException e) {
            return ajaxDoneError(e.getMessage());
        }
        return model;
    }

}
