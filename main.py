# -*- coding: utf-8 -*-
import model, data, genResult
import os, sys, pickle
from flask import Flask, jsonify, make_response, request, send_from_directory, after_this_request

reload(sys)
sys.setdefaultencoding('utf8')
app = Flask(__name__, static_folder = 'htmlclient')
app.json_encoder = model.MyJSONEncoder

def loadTable(tableName):
    table = model.Table(tableName)
    try:
        f = open("tables/{0}".format(tableName))
        table = pickle.load(f)
        f.close()
    except IOError, EOFError:
        table = model.Table(tableName)
        for datarow in data.getRows(tableName):
            row = model.Rows(datarow)
            table.addRow(row)
            if row.primaryKey:
                if table.primaryRow != None:
                    raise SyntaxError('两个主键 {0}, {1}'.format(table.primaryRow.dbName, row.dbName))
                table.primaryRow = row

        # 保存当前table配置
        f = open("tables/{0}".format(table.name), 'w')
        pickle.dump(table, f)
        f.close()
    return table

# 返回数据库中的所有表
@app.route('/db/table')
def getTable():
    return jsonify(tables = data.getTables())

# 返回指定表的所有字段
@app.route('/db/table/<tableName>')
def getRows(tableName):
    table = loadTable(tableName)
    return jsonify({
        "table": table,
        "rows": table.rows
    })

@app.route('/update/table/<tableName>', methods=['POST'])
def updateTable(tableName):
    jsonVal = request.get_json()
    field = jsonVal['field']
    val = jsonVal['val']

    table = loadTable(tableName)
    table.updateTable(field, val)

    # 保存当前table配置
    f = open("tables/{0}".format(table.name), 'w')
    pickle.dump(table, f)
    f.close()
    return jsonify(data = u'保存成功')

# 保存某个表的字段信息
@app.route('/db/table/<tableName>/save', methods=['POST'])
def saveRow(tableName):
    jsonVal = request.get_json()

    dbName = jsonVal['dbName']
    dbType = jsonVal['dbType']
    javaName = jsonVal['javaName']
    javaType = jsonVal['javaType']
    description = jsonVal['description']
    length = jsonVal['length']
    allowEmpty = jsonVal['allowEmpty']

    table = loadTable(tableName)
    table.updateTableRow(dbName, javaName, javaType, description, length, allowEmpty)

    # 保存当前table配置
    f = open("tables/{0}".format(table.name), 'w')
    pickle.dump(table, f)
    f.close()

    return make_response(u'保存成功', 200)

@app.route('/generate/<tableName>')
def generate(tableName):
    table = loadTable(tableName)
    genResult.generateFile(table)
    return jsonify({
        "voName": table.voName,
        "beanName": table.beanName
    })

@app.route('/download')
def download():
    fileName = request.args['fileName']
    @after_this_request
    def add_header(response):
        _, name = os.path.split(fileName)
        response.headers['Content-Disposition'] = 'attachment; filename={0}'.format(name)
        return response
    return send_from_directory('result', fileName, attachment_filename=fileName)

@app.route('/static/<path:filename>')
def static_file(filename):
    return send_from_directory('htmlclient/target', filename)

@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    print path
    return app.send_static_file('target/real_index.html')

if __name__ == '__main__':
    app.debug = True
    app.run()
