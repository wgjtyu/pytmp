package cn.com.harme.service.impl;

import cn.com.harme.common.util.DateUtil;
import cn.com.harme.common.util.ObjectUtils;
import cn.com.harme.entity.{{config.beanName}};
import cn.com.harme.entity.mapper.{{config.beanName}}Mapper;
import cn.com.harme.enums.SysLogType;
import cn.com.harme.framework.exception.ServiceException;
import cn.com.harme.service.{{config.voName}}Service;
import cn.com.harme.vo.search.BaseConditionVO;
import cn.com.harme.vo.{{config.voName}};
import cn.com.harme.vo.User;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(rollbackFor = Exception.class)
@Service({{config.voName}}Service.SERVICE_NAME)
public class {{config.voName}}ServiceImpl extends AbstractBusinessObjectServiceImpl implements {{config.voName}}Service {
    @Autowired
    private {{config.beanName}}Mapper {{config.beanName|getVarName}}Mapper;

    @Override
    public void add{{config.voName}}({{config.voName}} {{config.voName|getVarName}}, User loginUser) throws ServiceException {
        logger.debug("add{{config.voName}} param[{{config.voName|getVarName}}=" + {{config.voName|getVarName}} + "]");

        {{config.beanName}} bean = {{config.voName|getVarName}}.get{{config.beanName}}();
        bean.setCreateTime(DateUtil.currentTimestamp());
        bean.setEditorId(loginUser.getId());
        bean.setEditorName(loginUser.getName());
        {{config.beanName|getVarName}}Mapper.insert(bean);

        super.saveSysLog("添加{{config.description}}", bean.toString(), SysLogType.ADD, "", loginUser);
    }

    @Override
    public void update{{config.voName}}({{config.voName}} {{config.voName|getVarName}}, User loginUser) throws ServiceException {
        logger.debug("update{{config.voName}} param[{{config.voName|getVarName}}=" + {{config.voName|getVarName}} + "]");

        {{config.beanName}} bean = {{config.voName|getVarName}}.get{{config.beanName}}();
        bean.setCreateTime(DateUtil.currentTimestamp());
        bean.setEditorId(loginUser.getId());//操作管理员ID
        bean.setEditorName(loginUser.getName());
        {{config.beanName|getVarName}}Mapper.update(bean);

        super.saveSysLog("编辑{{config.description}}", bean.toString(), SysLogType.UPDATE, "", loginUser);
    }

    @Override
    public {{config.voName}} get{{config.voName}}({{config.primaryRow.javaType}} {{config.primaryRow.javaName}}) throws ServiceException {
        logger.debug("get{{config.voName}} param[{{config.primaryRow.javaName}}=" + {{config.primaryRow.javaName}} + "]");

        {{config.beanName}} bean = {{config.beanName|getVarName}}Mapper.load({{config.primaryRow.javaName}});
        if(ObjectUtils.isEmpty(bean)) {
            throw new ServiceException(getMessage("msg.result.empty"));
        }
        return new {{config.voName}}(bean);
    }

    @Override
    public void delete{{config.voName}}({{config.primaryRow.javaType}} {{config.primaryRow.javaName}}, User loginUser) throws ServiceException {
        logger.debug("delete{{config.voName}} param[{{config.primaryRow.javaName}}=" + {{config.primaryRow.javaName}} + "]");
        {{config.beanName|getVarName}}Mapper.delete({{config.primaryRow.javaName}});
        super.saveSysLog("删除{{config.description}}", "{{config.primaryRow.javaName}}=" + {{config.primaryRow.javaName}}, SysLogType.DELETE, "", loginUser);
    }

    @Override
    public List<{{config.voName}}> search(BaseConditionVO vo) {
        logger.debug("search param[vo=" + vo + "]");

        List<{{config.voName}}> {{config.voName|getVarName}}List = new ArrayList<{{config.voName}}>();
        RowBounds rb = new RowBounds(vo.getStartIndex(), vo.getPageSize());
        List<{{config.beanName}}> {{config.beanName|getVarName}}List = {{config.beanName|getVarName}}Mapper.findPageBreakByCondition(vo, rb);
        vo.setTotalCount({{config.beanName|getVarName}}Mapper.findNumberByCondition(vo));

        for ({{config.beanName}} bean : {{config.beanName|getVarName}}List) {
            {{config.voName|getVarName}}List.add(new {{config.voName}}(bean));
        }
        return {{config.voName|getVarName}}List;
    }

    @Override
    public List<{{config.voName}}> findAlls() {
        List<{{config.beanName}}> {{config.beanName|getVarName}}List = {{config.beanName|getVarName}}Mapper.findAll();
        List<{{config.voName}}> {{config.voName|getVarName}}List = new ArrayList<{{config.voName}}>();
        for ({{config.beanName}} bean: {{config.beanName|getVarName}}List) {
            {{config.voName|getVarName}}List.add(new {{config.voName}}(bean));
        }
        return {{config.voName|getVarName}}List;
    }

}
