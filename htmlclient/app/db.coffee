db = {}

#the view-model tracks a running list of todos,
#stores a description for new todos before they are created
#and takes care of the logic surrounding when adding is permitted
#and clearing the input after adding a todo to the list
db.vm = {}
db.vm.init = ->
    this.list = new Array()
    m.request({method: "GET", url: "/db/table"}).then (result) =>
        if result.tables != undefined then this.list = result.tables
    this.filter = m.prop ""

# the controller defines what part of the model is relevant for the current page
# in our case, there's only one view-model that handles everything
db.controller = -> db.vm.init()

# here's the view
db.view = ->
    m "html", [
        m "body", [
            m "div.center-container.list-group", [
                m "input", {
                    onkeyup: m.withAttr("value", db.vm.filter)
                    value: db.vm.filter()
                    placeholder: "输入名称进行过滤"
                }
                db.vm.list.map (i) ->
                    if i.indexOf(db.vm.filter()) != -1
                        m "a.list-group-item[href='table/#{i}']", {config: m.route}, i
            ]
        ]
    ]
