<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>

<c:import url="../pager/pagerForm.jsp"></c:import>

<form id="{{config.voName|getVarName}}ListForm" rel="pagerForm" onsubmit="return navTabSearch(this, '{{config.voName|getVarName}}LiNav')" method="post"
  action="<c:url value='/{{config.type}}/{{config.voName|getVarName}}?navTabId={{config.voName|getVarName}}LiNav'/>">
  <input type="hidden" name="pageNum" value="${vo.pageNum}"/>
  <input type="hidden" name="pageSize" value="${vo.pageSize}"/>
  <div class="pageHeader">
    <div class="searchBar">
      <table class="searchContent">
        <tbody>
          <tr>
            {% for row in rows|getSearch -%}
            <td>
              {{row.description}}：<input type="text" name="{{row.javaName}}" value="{{'${param.' + row.javaName + '}'}}" style="width:80px;"/>
            </td>
            <td>&nbsp;</td>
            {%- endfor %}
            <td>
              <ul>
                <li><div class="buttonActive"><div class="buttonContent"><button type="submit">搜索</button></div></div></li>
              </ul>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</form>

<c:set var="resourceUrls" value="${contextUser.resourceURLs['{{config.type|upper}}']}"/>

<div class="pageContent">
  <div class="panelBar">
    <ul class="toolBar">
      <c:if test="${fn:contains(resourceUrls,'/{{config.type}}/{{config.voName|getVarName}}/insert')}">
      <li><a class="add" target="navTab" rel="{{config.voName}}AddNav" title="添加{{config.description}}"
          href="<c:url value='/{{config.type}}/{{config.voName|getVarName}}/insert'/>"><span>添加</span></a></li>
      </c:if>
      <c:if test="${fn:contains(resourceUrls,'/{{config.type}}/{{config.voName|getVarName}}/edit')}">
      <li><a class="edit" target="navTab" rel="{{config.voName}}EditNav" warn="请选择{{config.description}}" title="编辑{{config.description}}"
          href="<c:url value='/{{config.type}}/{{config.voName|getVarName}}/edit/{slt_{{config.voName}}Id}'/>"><span>编辑</span></a></li>
      </c:if>
      <c:if test="${fn:contains(resourceUrls,'/{{config.type}}/{{config.voName|getVarName}}/delete')}">
      <li><a class="delete" target="ajaxTodo" warn="请选择{{config.description}}" title="你确定要删除吗?"
          href="<c:url value='/{{config.type}}/{{config.voName|getVarName}}/delete/{slt_{{config.voName}}Id}'/>">
          <span>删除</span>
      </a></li>
      </c:if>
      <c:if test="${fn:contains(resourceUrls,'/{{config.type}}/{{config.voName|getVarName}}/view')}">
      <li class="line">line</li>
      <li><a class="view" target="navTab" rel="{{config.voName}}ViewNav" warn="请选择{{config.description}}" title="{{config.description}}详情"
          href="<c:url value='/{{config.type}}/{{config.voName|getVarName}}/view/{slt_{{config.voName}}Id}'/>"><span>查看详情</span></a></li>
      </c:if>
      <li class="line">line</li>
    </ul>
  </div>
  <table class="table" width="100%" layoutH="116">
    <thead>
      <tr>
        <th width="50">序号</th>
        {% for row in rows|getSearch -%}
        {%- if row.javaType == 'Timestamp' -%}
        <th width="150" orderField="{{row.dbName}}" class="${param.orderField eq '{{row.dbName}}' ? param.orderDirection : ''}">{{row.description}}</th>
        {%- else -%}
        <th width="" orderField="{{row.dbName}}" class="${param.orderField eq '{{row.dbName}}' ? param.orderDirection : ''}">{{row.description}}</th>
        {% endif %}
        {%- endfor %}
      </tr>
    </thead>
    <tbody>
      <c:forEach var="{{config.voName|getVarName}}" items="{{ '${' + config.voName|getVarName + 'List}'}}" varStatus="s">
      <tr target="slt_{{config.voName}}Id" rel="{{'${'+ config.voName|getVarName + '.' + config.primaryRow.javaName + '}'}}">
        <td>${s.index+1}</td>
        {% for row in rows|getSearch -%}
        {%- if row.javaType == 'Timestamp' -%}
        <td><fmt:formatDate value="{{'${' + config.voName|getVarName + '.' + row.javaName + '}'}}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
        {%- else -%}
        <td>{{'${' + config.voName|getVarName +'.' + row.javaName + '}'}}</td>
        {% endif -%}
        {%- endfor %}
      </tr>
      </c:forEach>
    </tbody>
  </table>

  <c:import url="../pager/panelBar.jsp"></c:import>
</div>
