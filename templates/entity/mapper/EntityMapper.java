package cn.com.harme.entity.mapper;

import cn.com.harme.entity.{{config.beanName}};
import cn.com.harme.vo.search.BaseConditionVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface {{config.beanName}}Mapper extends BaseMapper<{{config.beanName}}, {{config.primaryRow.javaType}}> {

    /**
     * 查询，带分页功能
     */
    public List<{{config.beanName}}> findPageBreakByCondition(BaseConditionVO vo,
                                                           RowBounds rb);

    /**
     * 查询，带分页功能，统计查询结果总数
     */
    public Integer findNumberByCondition(BaseConditionVO vo);

}
