<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>

<div class="pageContent">
  <div class="pageFormContent" layoutH="57" style="overflow-x:hidden;">
    <table style="border-left: 1px solid #CCC;border-top: 1px solid #CCC;" width="98%">
{%- for row in rows|removePrimaryKey -%}
{% if loop.index0|isTwo %}
      <tr>
{%- endif %}
{%- if row.javaName == 'remark' -%}
        <td class="td-head">
          备注：
        </td>
        <td class="td-data" colspan="3">
          <span class="info" style="width:80%">{{'${' + config.voName|getVarName + '.remark}'}}</span>
        </td>
{% elif row.javaType == 'Timestamp' %}
        <td class="td-head">
          {{row.description}}：
        </td>
        <td class="td-data">
          <span class="info"><fmt:formatDate value="{{'${' + config.voName|getVarName + '.' + row.javaName + '}'}}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
        </td>
{% else %}
        <td class="td-head">
          {{row.description}}：
        </td>
        <td class="td-data">
          <span class="info">{{'${' + config.voName|getVarName + '.' + row.javaName + '}'}}</span>
        </td>
{%- endif %}
{%- if loop.last or loop.index|isTwo %}
      </tr>
{%- endif %}
{%- endfor %}
    </table>
  </div>
  <div class="formBar">
    <ul>
      <li><div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div></li>
    </ul>
  </div>
</div>
