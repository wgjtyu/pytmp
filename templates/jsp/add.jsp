<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%><%@ include file="/include.inc.jsp"%>

<div class="pageContent">
  <form id="{{config.voName|getVarName}}AddForm" action="<c:url value='/{{config.type}}/{{config.voName|getVarName}}/insert?navTabId={{config.voName|getVarName}}LiNav&callbackType=closeCurrent'/>"
    method="post" class="pageForm required-validate" onsubmit="return validateCallback(this,navTabAjaxDone);">
    <div class="pageFormContent" layoutH="57" style="overflow-x:hidden;">
      <table style="border-left: 1px solid #CCC;border-top: 1px solid #CCC;" width="98%">
{%- for row in rows|getNormalRow -%}
{% if loop.index0|isTwo %}
        <tr>
{%- endif %}
{%- if row.javaName == 'remark' -%}
          <td class="td-head">
            备注：
          </td>
          <td class="td-data" colspan="3">
            <textarea name="remark" class="textareaInfo" maxlength="{{row.length}}"></textarea>
          </td>
{% else %}
          <td class="td-head">
            {{row.description}}：
          </td>
          <td class="td-data">
            <input type="text" name="{{row.javaName}}"{% if not row.allowEmpty %} class="required"{% endif %}{% if row.length>0 %} maxlength="{{row.length}}"{% endif %}/>
          </td>
{%- endif %}
{%- if loop.last or loop.index|isTwo %}
        </tr>
{%- endif %}
{%- endfor %}
      </table>
    </div>
    <div class="formBar">
      <ul>
        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
        <li><div class="button"><div class="buttonContent"><button type="button" class="close">关闭</button></div></div></li>
      </ul>
    </div>
  </form>
</div>
