package cn.com.harme.vo;

import cn.com.harme.common.util.EnumUtils;
import cn.com.harme.common.util.ObjectUtils;
import cn.com.harme.common.util.StringUtils;
import cn.com.harme.entity.{{config.beanName}};

import java.sql.Timestamp;

public class {{config.voName}} extends AbstractBusinessObject {
    // TODO 生成UUID

    private {{config.beanName}} {{config.beanName|getVarName}};

    public {{config.voName}}() {
        this.{{config.beanName|getVarName}} = new {{config.beanName}}();
    }

    public {{config.voName}}({{config.beanName}} {{config.beanName|getVarName}}) {
        this.{{config.beanName|getVarName}} = {{config.beanName|getVarName}};
    }

    public {{config.beanName}} get{{config.beanName}}() {
        return this.{{config.beanName|getVarName}};
    }
{% for row in rows %}
    public {{row.javaType}} get{{row.gsetName}}() {
        return this.{{config.beanName|getVarName}}.get{{row.gsetName}}();
    }

    {% if row.javaType == "String" %}
    public void set{{row.gsetName}}({{row.javaType}} {{row.javaName}}) {
        this.{{config.beanName|getVarName}}.set{{row.gsetName}}(StringUtils.trim({{row.javaName}}));
    }
    {% else %}
    public void set{{row.gsetName}}({{row.javaType}} {{row.javaName}}) {
        this.{{config.beanName|getVarName}}.set{{row.gsetName}}({{row.javaName}});
    }
    {% endif %}
{% endfor %}
    public {{config.primaryRow.javaType}} getId() {
        return this.{{config.beanName|getVarName}}.get{{config.primaryRow.gsetName}}();
    }
}
