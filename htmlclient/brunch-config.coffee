stylus = require 'stylus'

exports.config =
  modules:
      definition: false
      wrapper: false
  paths:
      public: 'target'
      watched: ['app', 'assets', 'dev']
  files:
    javascripts:
      defaultExtension: 'coffee'
      joinTo:
        'app.js': /^app/
        'dev.js': /^dev/
        'vendor.js': /^bower_components/
      order:
        after: ['app/main.coffee']
    stylesheets:
      defaultExtension: 'styl'
      joinTo:'app.css'
