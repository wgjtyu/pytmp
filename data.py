# -*- coding: utf-8 -*-
__author__ = 'wgjtyu'
import pymssql

dbName = 'czhb_oa_dev'

# 获取所有表名
def getTables():
    conn = pymssql.connect(host="172.16.10.75", user="sa", password="sa", database=dbName)
    cur = conn.cursor()
    cur.execute("select name from sys.tables where type = 'U';")

    row = cur.fetchone()
    result = []
    while row:
        result.append(row[0])
        row = cur.fetchone()
    conn.close()
    return result


# 获取表中的每个字段
def getRows(tableName):
    conn = pymssql.connect(host="172.16.10.75", user="sa", password="sa", database=dbName)
    cur = conn.cursor()

    cur.execute("""
    SELECT col.name AS ColumnName, col.isnullable AS IsNullable, col.length AS DataLength, tp.name AS DataType, ep.value AS Description,
    (
        SELECT COUNT(0) FROM sys.sysobjects
        WHERE parent_obj=obj.id
        AND name=(
            SELECT TOP 1 name FROM sys.sysindexes ind
            inner join sys.sysindexkeys indkey
            on ind.indid=indkey.indid
            and indkey.colid=col.colid
            and indkey.id=obj.id
            where ind.id=obj.id
            and ind.name like 'PK_%'
        )
    ) AS IsPrimaryKey
    FROM sys.sysobjects obj
    INNER JOIN sys.syscolumns col ON obj.id = col.id
    LEFT JOIN sys.systypes tp ON col.xtype=tp.xusertype
    LEFT JOIN sys.extended_properties ep ON ep.major_id=obj.id AND ep.minor_id=col.colid AND ep.name='MS_Description'
    WHERE obj.name='{0}'
    """.format(tableName))

    row = cur.fetchone()
    rows = []
    while row:
        aRow = []
        aRow.append(row[0]) #字段名称
        aRow.append('') #(row[4]) #字段描述

        # 字段类型
        if row[3].upper() == u'DATETIME': # 将MSSQL的类型转换到JDBC类型
            aRow.append(u'TIMESTAMP')
        elif row[3].upper() == u'INT':
            aRow.append(u'INTEGER')
        else:
            aRow.append(row[3].upper())

        if(row[3] in ['varchar', 'nvarchar', 'ntext']): # 长度限制
            aRow.append(row[2])
        else:
            aRow.append(None)
        if(row[1] == 1): # 可以为空
            aRow.append(True)
        else:
            aRow.append(False)
        aRow.append(None) # 默认值
        aRow.append(None) # 可以搜索
        if row[5] == 1: # 主键
            aRow.append(True)
        else:
            aRow.append(False)

        rows.append(aRow)
        row = cur.fetchone()
    conn.close()
    return rows
