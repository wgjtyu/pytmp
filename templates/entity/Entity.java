package cn.com.harme.entity;

import java.io.Serializable;
import java.sql.Timestamp;

// {{config.description}}
public class {{config.beanName}} extends AbstractDO {
    // TODO 生成UUID

{% for row in rows %}
    private {{row.javaType}} {{row.javaName}}; // {{row.description}}{% endfor %}
{% for row in rows %}
    public {{row.javaType}} get{{row.gsetName}}() {
        return this.{{row.javaName}};
    }

    public void set{{row.gsetName}}({{row.javaType}} {{row.javaName}}) {
        this.{{row.javaName}} = {{row.javaName}};
    }
{% endfor %}
    @Override
    public Serializable getId() {
        return {{config.primaryRow.javaName}};
    }
}
