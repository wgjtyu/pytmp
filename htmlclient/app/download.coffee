download = {}

download.controller = ->
    this.voName = m.route.param("voName")
    this.varName = this.voName[0].toLowerCase() + this.voName.slice(1)
    this.beanName = m.route.param("beanName")

    this

download.view = (ctrl) ->
    m "html", [
        m "body", [
            m "div.center-container#file-download", [
                m "div", "点击文件名，下载对应的生成文件"
                m "a.btn.btn-primary", {href: "/download?fileName=entity/#{ctrl.beanName}.java"}, "#{ctrl.beanName}.java"

                m "a.btn.btn-primary", {href: "/download?fileName=entity/mapper/#{ctrl.beanName}Mapper.xml"}, "#{ctrl.beanName}Mapper.xml"
                m "a.btn.btn-primary", {href: "/download?fileName=entity/mapper/#{ctrl.beanName}Mapper.java"}, "#{ctrl.beanName}Mapper.java"

                m "a.btn.btn-primary", {href: "/download?fileName=vo/#{ctrl.voName}.java"}, "#{ctrl.voName}.java"

                m "a.btn.btn-primary", {href: "/download?fileName=service/#{ctrl.voName}Service.java"}, "#{ctrl.voName}Service.java"
                m "a.btn.btn-primary", {href: "/download?fileName=service/impl/#{ctrl.voName}ServiceImpl.java"}, "#{ctrl.voName}ServiceImpl.java"
                m "a.btn.btn-primary", {href: "/download?fileName=#{ctrl.voName}Controller.java"}, "#{ctrl.voName}Controller.java"

                m "a.btn.btn-primary", {href: "/download?fileName=jsp/#{ctrl.varName}-add.jsp"}, "#{ctrl.varName}-add.jsp"
                m "a.btn.btn-primary", {href: "/download?fileName=jsp/#{ctrl.varName}-edit.jsp"}, "#{ctrl.varName}-edit.jsp"
                m "a.btn.btn-primary", {href: "/download?fileName=jsp/#{ctrl.varName}-view.jsp"}, "#{ctrl.varName}-view.jsp"
                m "a.btn.btn-primary", {href: "/download?fileName=jsp/#{ctrl.varName}-list.jsp"}, "#{ctrl.varName}-list.jsp"
            ]
        ]
    ]
