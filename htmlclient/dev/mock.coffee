window.XMLHttpRequest = new( ->
    request = ->
        this.$headers = {}
        this.setRequestHeader = (key, value) ->
            this.$headers[key] = value
        this.open = (method, url) ->
            this.method = method
            this.url = url
            null
        this.send = ->
            if this.url == 'db/table' and this.method == 'GET'
                this.responseText = JSON.stringify {tables: [{tableName: '123'}, {tableName: '234'}]}
            else if /db\/table\/\d+/.test(this.url) and this.method == 'GET'
                this.responseText = JSON.stringify {rows: [
                    {dbName: 'dn1', javaName: 'jn1', dbType: 'dt1', javaType: 'jt1', description: 'des1', length: 10}
                    {dbName: 'dn2', javaName: 'jn2', dbType: 'dt2', javaType: 'jt2', description: 'des2', length: 20}
                ]}
            else
                console.log this.method, this.url
                this.responseText = JSON.stringify this
            this.readyState = 4
            this.status = 200
            this.onreadystatechange() # send fake result to client
            request.$instances.push this
        null
    request.$instances = []
    return request
)
