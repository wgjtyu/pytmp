# -*- coding: utf-8 -*-
__author__ = 'wgjtyu'
from flask.json import JSONEncoder

typeTable = { # JDBC类型转换到Java类型
    u'FLOAT': 'Double',
    u'BIGINT': 'Long',
    u'INTEGER': 'Integer',
    u'CHAR': 'String',
    u'TEXT': 'String',
    u'VARCHAR': 'String',
    u'NVARCHAR': 'String',
    u'LONGVARCHAR': 'String',
    u'TIMESTAMP': 'Timestamp',
    u'SMALLDATETIME': 'Date'
}

class Table:

    def __init__(self, name):
        self.name = name

        words = name.split('_')
        lowerWords = [w.lower() for w in words]
        resultWords = map(lambda w: w[0].upper() + w[1:], lowerWords)

        self.beanName = ''.join(resultWords)
        self.voName = self.beanName
        self.type = 'config'
        self.description = u'描述'
        self.primaryRow = None
        self.rows = []

    def addRow(self, row):
        self.rows.append(row)

    def updateTable(self, field, val):
        if field == 'beanName':
            self.beanName = val
        elif field == 'description':
            self.description = val
        elif field == 'voName':
            self.voName = val
        elif field == 'type':
            self.type = val
        else:
            print 'unknown filed: {0}'.format(field)

    def updateTableRow(self, dbName, javaName, javaType, description,
                    length, allowEmpty, default = False, search = False):
        row = filter(lambda r:r.isThisRow(dbName), self.rows)
        assert len(row) == 1
        row[0].javaName = javaName
        row[0].javaType = javaType
        row[0].description = description
        row[0].length = length
        row[0].allowEmpty = allowEmpty


class Rows:

    def __init__(self, row):
        self.dbName = row[0]
        self.description = row[1]
        if row[2] == 'NTEXT':
            row[2] = 'LONGVARCHAR'
        elif row[2] == 'MONEY':
            row[2] = 'FLOAT'
        self.dbType = row[2]
        self.length = row[3]
        self.allowEmpty = row[4]
        self.default = row[5]
        self.search = row[6]
        self.primaryKey = row[7]

        self.javaType = typeTable[self.dbType]
        self.setJavaRowName()

    def __str__(self):
        return self.javaName

    def setJavaRowName(self):
        words = self.dbName.split('_')
        lowerWords = [w.lower() for w in words]
        resultWords = map(lambda w: w[0].upper() + w[1:], lowerWords[1:])

        self.javaName = lowerWords[0] + ''.join(resultWords)
        self.gsetName = self.javaName[0].upper() + self.javaName[1:]

    def isThisRow(self, dbName):
        return self.dbName == dbName


class MyJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Rows):
            return {
                'dbName': obj.dbName,
                'javaName': obj.javaName,
                'dbType': obj.dbType,
                'javaType': obj.javaType,
                'description': obj.description,
                'length': obj.length,
                'allowEmpty': obj.allowEmpty,
                'search': obj.search,
                'primaryKey': obj.primaryKey
                }
        elif isinstance(obj, Table):
            result = {
                'name': obj.name,
                'beanName': obj.beanName,
                'type': obj.type,
                'voName': obj.voName,
                'description': obj.description,
                'primaryRow': obj.primaryRow,
            }
            # for row in obj.rows:
            #     result['rows'].append({
            #         'dbName': obj.dbName,
            #         'javaName': obj.javaName,
            #         'dbType': obj.dbType,
            #         'javaType': obj.javaType,
            #         'description': obj.description,
            #         'length': obj.length,
            #         'allowEmpty': obj.allowEmpty,
            #         'search': obj.search,
            #         'primaryKey': obj.primaryKey
            #     })
            return result

        return super(MyJSONEncoder, self).default(obj)
